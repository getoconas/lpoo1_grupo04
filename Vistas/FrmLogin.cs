﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmLogin : Form
    {
        private List<Rol> listaRol = new List<Rol>();
        private List<Usuario> listaUsuarios = new List<Usuario>();

        public FrmLogin()
        {
            InitializeComponent();
            cargarListaUsuarios();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            FrmMain frmMain = new FrmMain();
            Usuario usuario = new Usuario();

            if (buscarUsuario(txtUserName.Text, txtPassword.Text))
            {
                usuario = obtenerUsuario(txtUserName.Text, txtPassword.Text);
                MessageBox.Show("Bienvenido: " + txtUserName.Text+"\nRol: "+obtenerRol(usuario.Rol_Codigo),"Sistema",MessageBoxButtons.OK, MessageBoxIcon.Information);
                frmMain.Show();
                this.Hide();
            }
            else 
                MessageBox.Show("Datos Erróneos. Por favor, inténtelo otra vez.", "Sistema",MessageBoxButtons.OK,MessageBoxIcon.Error);

        }

        private void cargarListaUsuarios() 
        { 
            Rol rol1 = new Rol(1, "Administrador");
            Rol rol2 = new Rol(2, "Operador");
            Rol rol3 = new Rol(3, "Auditor");
            listaRol.Add(rol1);
            listaRol.Add(rol2);
            listaRol.Add(rol3);

            Usuario usuario1 = new Usuario("admin", "admin",rol1.Rol_Codigo);
            Usuario usuario2 = new Usuario("operador", "operador",rol2.Rol_Codigo);
            Usuario usuario3 = new Usuario("auditor", "auditor",rol3.Rol_Codigo);

            listaUsuarios.Add(usuario1);
            listaUsuarios.Add(usuario2);
            listaUsuarios.Add(usuario3);
        }

        private bool buscarUsuario(string nombreUsuario, string password)
        {
            Boolean encontrado = false;
            foreach (Usuario u in listaUsuarios) 
            {
                if (u.Usu_NombreUsuario.Equals(nombreUsuario) && u.Usu_Password.Equals(password)) 
                {
                    encontrado = true; 
                    break;
                }   
            }
            return encontrado;
        }

        private Usuario obtenerUsuario(string nombreUsuario, string password) 
        {
            Usuario usuario = null;
            foreach (Usuario u in listaUsuarios)
            {
                if (u.Usu_NombreUsuario.Equals(nombreUsuario) && u.Usu_Password.Equals(password))
                {
                    usuario = u; 
                    break;
                }
            }
            return usuario;
        }

        private string obtenerRol(int codigoRol) 
        {
            string rol = null;
            foreach(Rol r in listaRol)
            {
                if (r.Rol_Codigo.Equals(codigoRol))
                {
                    rol = r.Rol_Descripcion;
                    break;
                }
            }

            return rol;
        }

        private void btnIngresar_MouseHover(object sender, EventArgs e)
        {
            btnIngresar.BackColor = System.Drawing.Color.Gold;
        }

        private void btnIngresar_MouseLeave(object sender, EventArgs e)
        {
            btnIngresar.BackColor = System.Drawing.Color.Transparent;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

    }
}
