﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmGestionUsuarios : Form
    {
        private FrmMain frmMain;
        private FrmAltaUsuario frmAltaUsuario;
        private Usuario usuario;

        private static FrmGestionUsuarios instancia;

        public FrmGestionUsuarios()
        {
            InitializeComponent();
        }

        public static FrmGestionUsuarios GetInstancia()
        {
            if (instancia == null)
                instancia = new FrmGestionUsuarios();
            return instancia;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAltaUsuario = FrmAltaUsuario.GetInstancia();
            frmAltaUsuario.Show();
            this.Hide();
        }

        private void FrmGestionUsuarios_Load(object sender, EventArgs e)
        {
            cargar_Usuarios();
        }

        public void cargar_Usuarios()
        {
            dgwUsuarios.DataSource = GestionUsuario.list_Usuarios();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (txtApellido.Text != "")
            {
                dgwUsuarios.DataSource = GestionUsuario.buscar_Usuarios(txtApellido.Text);
            }
            else
            {
                cargar_Usuarios();
            }
        }

        private void FrmGestionUsuarios_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmMain = FrmMain.GetInstancia();
            frmMain.Show();
            instancia = null;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            FrmAltaUsuario frmAltaUsuario = FrmAltaUsuario.GetInstancia();
            frmAltaUsuario.Modificar_Usuario(this.usuario);
            frmAltaUsuario.Show();
            this.Hide();
        }

        private void dgwUsuarios_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgwUsuarios.CurrentRow != null) 
           {
               try
               {
                   usuario = new Usuario();
                   usuario.Usu_Id = (int)dgwUsuarios.CurrentRow.Cells["ID"].Value;
                   usuario.Usu_NombreUsuario = dgwUsuarios.CurrentRow.Cells["Usuario"].Value.ToString();
                   usuario.Usu_Password = dgwUsuarios.CurrentRow.Cells["Contraseña"].Value.ToString();
                   usuario.Usu_Apellido = dgwUsuarios.CurrentRow.Cells["Apellido"].Value.ToString();
                   usuario.Usu_Nombre = dgwUsuarios.CurrentRow.Cells["Nombre"].Value.ToString();

                   usuario.Rol_Codigo = (int)dgwUsuarios.CurrentRow.Cells["Id_Rol"].Value;
               }
               catch 
               {
                   MessageBox.Show("No selecciono ningun usuario");
               }
               

           }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult opcion;
            opcion =  MessageBox.Show("Desea Eliminar al Usuario: " + this.usuario.Usu_NombreUsuario+" ?", "Eliminar Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if(opcion.Equals(DialogResult.Yes))
            {
                GestionUsuario.delete_Usuario(usuario);
                MessageBox.Show("Usuario eliminado Correctamente!!","Eliminar Usuario",MessageBoxButtons.OK,MessageBoxIcon.Information);
                cargar_Usuarios();
            } 
        }
    }
}
