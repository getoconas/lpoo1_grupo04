﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmAltaUsuario : Form
    {
        private FrmGestionUsuarios frmGestionUsuario;
        private static FrmAltaUsuario instancia;

        public FrmAltaUsuario()
        {
            InitializeComponent();
        }

        public static FrmAltaUsuario GetInstancia()
        {
            if (instancia == null)
                instancia = new FrmAltaUsuario();
            return instancia;
        }

        private void FrmAltaUsuario_Load(object sender, EventArgs e)
        {
            cargar_Roles();
        }

        private void cargar_Roles()
        {
            cmbRol.DisplayMember = "ROL_Descripcion";
            cmbRol.ValueMember = "ROL_Codigo";
            cmbRol.DataSource = GestionUsuario.list_Roles();
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            frmGestionUsuario = FrmGestionUsuarios.GetInstancia();
            Usuario usuario = new Usuario();
            usuario.Usu_NombreUsuario = txtUsuario.Text;
            usuario.Usu_Password = txtPassword.Text;
            usuario.Usu_Apellido = txtApellido.Text;
            usuario.Usu_Nombre = txtNombre.Text;
            usuario.Rol_Codigo = (int) cmbRol.SelectedValue;

            GestionUsuario.insert_Usuario(usuario);
            MessageBox.Show("Usuario Agregado con Exito", "Agregar Usuario", MessageBoxButtons.OK, MessageBoxIcon.Information);
            frmGestionUsuario.cargar_Usuarios();
            frmGestionUsuario.Show();
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
        }


        private void FrmAltaUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            instancia = null;
            frmGestionUsuario = FrmGestionUsuarios.GetInstancia();
            frmGestionUsuario.Show();
        }

        public void Modificar_Usuario(Usuario usuario) 
        {
            txtUsuario.Text = usuario.Usu_NombreUsuario;
            txtPassword.Text = usuario.Usu_Password;
            txtApellido.Text = usuario.Usu_Apellido;
            txtNombre.Text = usuario.Usu_Nombre;
            cmbRol.SelectedValue = usuario.Rol_Codigo.ToString();
        }
    }
}
