﻿namespace Vistas
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPrestamos = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDestinos = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPeriodos = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClientes = new System.Windows.Forms.Button();
            this.btnPeriodos = new System.Windows.Forms.Button();
            this.btnDestinos = new System.Windows.Forms.Button();
            this.btnGestionUsuarios = new System.Windows.Forms.Button();
            this.btnGestionPrestamo = new System.Windows.Forms.Button();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.menuClientes,
            this.menuPrestamos,
            this.menuDestinos,
            this.menuPeriodos});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(878, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSalir});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Sistema";
            // 
            // menuSalir
            // 
            this.menuSalir.Name = "menuSalir";
            this.menuSalir.Size = new System.Drawing.Size(96, 22);
            this.menuSalir.Text = "Salir";
            this.menuSalir.Click += new System.EventHandler(this.menuSalir_Click);
            // 
            // menuClientes
            // 
            this.menuClientes.Name = "menuClientes";
            this.menuClientes.Size = new System.Drawing.Size(61, 20);
            this.menuClientes.Text = "Clientes";
            this.menuClientes.Click += new System.EventHandler(this.menuClientes_Click);
            // 
            // menuPrestamos
            // 
            this.menuPrestamos.Name = "menuPrestamos";
            this.menuPrestamos.Size = new System.Drawing.Size(74, 20);
            this.menuPrestamos.Text = "Prestamos";
            this.menuPrestamos.Click += new System.EventHandler(this.menuPrestamos_Click);
            // 
            // menuDestinos
            // 
            this.menuDestinos.Name = "menuDestinos";
            this.menuDestinos.Size = new System.Drawing.Size(64, 20);
            this.menuDestinos.Text = "Destinos";
            this.menuDestinos.Click += new System.EventHandler(this.menuDestinos_Click);
            // 
            // menuPeriodos
            // 
            this.menuPeriodos.Name = "menuPeriodos";
            this.menuPeriodos.Size = new System.Drawing.Size(65, 20);
            this.menuPeriodos.Text = "Periodos";
            this.menuPeriodos.Click += new System.EventHandler(this.menuPeriodos_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnClientes.Image")));
            this.btnClientes.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClientes.Location = new System.Drawing.Point(23, 76);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Padding = new System.Windows.Forms.Padding(0, 5, 0, 10);
            this.btnClientes.Size = new System.Drawing.Size(224, 109);
            this.btnClientes.TabIndex = 1;
            this.btnClientes.Text = "Clientes";
            this.btnClientes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.btnClientes_Click);
            // 
            // btnPeriodos
            // 
            this.btnPeriodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPeriodos.Image = ((System.Drawing.Image)(resources.GetObject("btnPeriodos.Image")));
            this.btnPeriodos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPeriodos.Location = new System.Drawing.Point(279, 76);
            this.btnPeriodos.Name = "btnPeriodos";
            this.btnPeriodos.Padding = new System.Windows.Forms.Padding(0, 5, 0, 10);
            this.btnPeriodos.Size = new System.Drawing.Size(224, 109);
            this.btnPeriodos.TabIndex = 3;
            this.btnPeriodos.Text = "Periodos";
            this.btnPeriodos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPeriodos.UseVisualStyleBackColor = true;
            this.btnPeriodos.Click += new System.EventHandler(this.btnPeriodos_Click);
            // 
            // btnDestinos
            // 
            this.btnDestinos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDestinos.Image = ((System.Drawing.Image)(resources.GetObject("btnDestinos.Image")));
            this.btnDestinos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDestinos.Location = new System.Drawing.Point(23, 232);
            this.btnDestinos.Name = "btnDestinos";
            this.btnDestinos.Padding = new System.Windows.Forms.Padding(0, 5, 0, 10);
            this.btnDestinos.Size = new System.Drawing.Size(224, 109);
            this.btnDestinos.TabIndex = 4;
            this.btnDestinos.Text = "Destinos";
            this.btnDestinos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDestinos.UseVisualStyleBackColor = true;
            this.btnDestinos.Click += new System.EventHandler(this.btnDestinos_Click);
            // 
            // btnGestionUsuarios
            // 
            this.btnGestionUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGestionUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("btnGestionUsuarios.Image")));
            this.btnGestionUsuarios.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGestionUsuarios.Location = new System.Drawing.Point(541, 76);
            this.btnGestionUsuarios.Name = "btnGestionUsuarios";
            this.btnGestionUsuarios.Padding = new System.Windows.Forms.Padding(0, 5, 0, 10);
            this.btnGestionUsuarios.Size = new System.Drawing.Size(224, 109);
            this.btnGestionUsuarios.TabIndex = 7;
            this.btnGestionUsuarios.Text = "Gestion de Usuarios";
            this.btnGestionUsuarios.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGestionUsuarios.UseVisualStyleBackColor = true;
            this.btnGestionUsuarios.Click += new System.EventHandler(this.btnGestionUsuarios_Click);
            // 
            // btnGestionPrestamo
            // 
            this.btnGestionPrestamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGestionPrestamo.Image = ((System.Drawing.Image)(resources.GetObject("btnGestionPrestamo.Image")));
            this.btnGestionPrestamo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGestionPrestamo.Location = new System.Drawing.Point(279, 232);
            this.btnGestionPrestamo.Name = "btnGestionPrestamo";
            this.btnGestionPrestamo.Padding = new System.Windows.Forms.Padding(0, 5, 0, 10);
            this.btnGestionPrestamo.Size = new System.Drawing.Size(224, 109);
            this.btnGestionPrestamo.TabIndex = 8;
            this.btnGestionPrestamo.Text = "Gestion de Prestamos";
            this.btnGestionPrestamo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGestionPrestamo.UseVisualStyleBackColor = true;
            this.btnGestionPrestamo.Click += new System.EventHandler(this.btnGestionPrestamo_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(878, 450);
            this.Controls.Add(this.btnGestionPrestamo);
            this.Controls.Add(this.btnGestionUsuarios);
            this.Controls.Add(this.btnDestinos);
            this.Controls.Add(this.btnPeriodos);
            this.Controls.Add(this.btnClientes);
            this.Controls.Add(this.menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema Financiero";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuSalir;
        private System.Windows.Forms.ToolStripMenuItem menuClientes;
        private System.Windows.Forms.ToolStripMenuItem menuPrestamos;
        private System.Windows.Forms.ToolStripMenuItem menuDestinos;
        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.Button btnPeriodos;
        private System.Windows.Forms.Button btnDestinos;
        private System.Windows.Forms.ToolStripMenuItem menuPeriodos;
        private System.Windows.Forms.Button btnGestionUsuarios;
        private System.Windows.Forms.Button btnGestionPrestamo;
    }
}