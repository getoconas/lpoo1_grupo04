﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmAltaPago : Form
    {
        private Pago pago;
        public FrmAltaPago()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!validarCamposVacios())
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Desea Agregar el Pago?", "Agregar Pago", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion.Equals(DialogResult.OK))
                {
                    agregarPago();
                    MessageBox.Show(pago.ToString());
                }
                else
                    MessageBox.Show("Pago no Registrado!!", "Agregar Pago", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("Algunos Campos Estan Vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool validarCamposVacios()
        {
            return txtCodigo.Text.Equals("") || txtImporte.Text.Equals("");
        }

        private void agregarPago()
        {
            pago = new Pago();
            pago.Pag_Codigo = Convert.ToInt32(txtCodigo.Text);
            pago.Pag_Fecha = dtpFecha.Value;
            pago.Pag_Importe = Convert.ToDecimal(txtImporte.Text);

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        
    }
}
