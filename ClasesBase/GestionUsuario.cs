﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
    public class GestionUsuario
    {
        public static DataTable list_Roles()
        {
            SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Rol";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            //ejecuta la consulta
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Llena los datos de la consulta en el Datatable
            DataTable dt = new DataTable();
            da.Fill(dt);
            
            return dt;
        }

        public static void insert_Usuario(Usuario usuario)
        {
            SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO Usuario(USU_NombreUsuario, USU_Contraseña, USU_Apellido, USU_Nombre, ROL_Codigo) values(@usuario, @password, @apellido, @nombre, @rol)";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@usuario", usuario.Usu_NombreUsuario);
            cmd.Parameters.AddWithValue("@password", usuario.Usu_Password);
            cmd.Parameters.AddWithValue("@apellido", usuario.Usu_Apellido);
            cmd.Parameters.AddWithValue("@nombre", usuario.Usu_Nombre);
            cmd.Parameters.AddWithValue("@rol", usuario.Rol_Codigo);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void delete_Usuario(Usuario usuario) 
        {
            SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DELETE FROM Usuario WHERE USU_ID = @idUsuario";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@idUsuario", usuario.Usu_Id);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static DataTable list_Usuarios()
        {
            SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT ";
            cmd.CommandText += "USU_ID AS 'ID', ";
            cmd.CommandText += "USU_NombreUsuario AS 'Usuario', ";
            cmd.CommandText += "USU_Contraseña AS 'Contraseña', ";
            cmd.CommandText += "USU_Apellido AS 'Apellido', ";
            cmd.CommandText += "USU_Nombre AS 'Nombre', ";
            cmd.CommandText += "U.Rol_Codigo AS 'Id_Rol', ";
            cmd.CommandText += "ROL_Descripcion AS 'Rol' ";
            cmd.CommandText += "FROM Usuario AS U LEFT JOIN Rol AS R ON (R.Rol_Codigo = U.Rol_Codigo)";

            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            //ejecuta la consulta
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Llena los datos de la consulta en el Datatable
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }

        public static DataTable buscar_Usuarios(String apellido)
        {
            SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT ";
            cmd.CommandText += "USU_ID AS 'ID', ";
            cmd.CommandText += "USU_NombreUsuario AS 'Usuario', ";
            cmd.CommandText += "USU_Contraseña AS 'Contraseña', ";
            cmd.CommandText += "USU_Apellido AS 'Apellido', ";
            cmd.CommandText += "USU_Nombre AS 'Nombre', ";

            cmd.CommandText += "U.Rol_Codigo AS 'Id_Rol', ";
            cmd.CommandText += "ROL_Descripcion AS 'Rol' ";
            cmd.CommandText += "FROM Usuario AS U LEFT JOIN Rol AS R ON (R.Rol_Codigo = U.Rol_Codigo)";

            cmd.CommandText += " WHERE USU_Apellido LIKE @apellido ";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@apellido", "%" + apellido + "%");

            //Ejecuta la consulta
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Llena los datos de la consulta en el Datatable
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }
    }
}
