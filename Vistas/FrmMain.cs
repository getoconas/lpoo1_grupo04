﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vistas
{
    public partial class FrmMain : Form
    {
        private static  FrmMain instancia;

        public FrmMain()
        {
            InitializeComponent();
        }

        public static FrmMain GetInstancia() 
        {
            if (instancia == null)
                instancia = new FrmMain();
            return instancia;
        }

        private void menuClientes_Click(object sender, EventArgs e)
        {
            FrmAltaClientes frmAltaClientes = new FrmAltaClientes();
            frmAltaClientes.Show();
        }

        private void menuSalir_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            this.Close();
        }

        private void btnDestinos_Click(object sender, EventArgs e)
        {
            FrmAltaDestino frmAltaDestinos = new FrmAltaDestino();
            frmAltaDestinos.Show();
        }

        private void btnPeriodos_Click(object sender, EventArgs e)
        {
            FrmAltaPeriodos frmAltaPeriodos = new FrmAltaPeriodos();
            frmAltaPeriodos.Show();
        }

        private void menuDestinos_Click(object sender, EventArgs e)
        {
            FrmAltaDestino frmAltaDestinos = new FrmAltaDestino();
            frmAltaDestinos.Show();
        }

        private void menuPeriodos_Click(object sender, EventArgs e)
        {
            FrmAltaPeriodos frmAltaPeriodos = new FrmAltaPeriodos();
            frmAltaPeriodos.Show();
        }

        private void menuPrestamos_Click(object sender, EventArgs e)
        {
            FrmAltaPrestamos frmAltaPrestamos = new FrmAltaPrestamos();
            frmAltaPrestamos.Show();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            FrmAltaClientes frmAltaClientes = FrmAltaClientes.GetInstancia();
            frmAltaClientes.Show();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            instancia = null;
            Application.Exit();
        }

        private void btnGestionUsuarios_Click(object sender, EventArgs e)
        {
            FrmGestionUsuarios frmGestionUsuarios = FrmGestionUsuarios.GetInstancia();
            frmGestionUsuarios.Show();
            this.Hide();
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnGestionPrestamo_Click(object sender, EventArgs e)
        {
            FrmGestionPrestamos frmListadoPrestamo = FrmGestionPrestamos.GetInstancia();
            frmListadoPrestamo.Show();
            this.Hide();
        }
    }
}
