﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;
namespace Vistas
{
    public partial class FrmAltaDestino : Form
    {
        private Destino destino;
        public FrmAltaDestino()
        {
            InitializeComponent();
        }

 
        private bool validarCamposVacios()
        {
            return txtCodigo.Text.Equals("") || txtDescripcion.Text.Equals("");
        }

        private void agregarPeriodo()
        {
            destino = new Destino();
            destino.Des_Codigo = Convert.ToInt32(txtCodigo.Text);
            destino.Des_Descripcion = txtDescripcion.Text;

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!validarCamposVacios())
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Desea Agregar Destino?", "Agregar Destino", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion.Equals(DialogResult.OK))
                {
                    agregarPeriodo();
                    MessageBox.Show(destino.ToString());
                }
                else
                    MessageBox.Show("Periodo no Registrado!!", "Agregar Periodo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("Algunos Campos Estan Vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

 
      
    }
}
