﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmGestionPrestamos : Form
    {
        private FrmMain frmMain;
        private FrmAltaPrestamos frmAltaPrestamo;
        private static FrmGestionPrestamos instancia;

        public FrmGestionPrestamos()
        {
            InitializeComponent();
        }

        public static FrmGestionPrestamos GetInstancia()
        {
            if (instancia == null)
                instancia = new FrmGestionPrestamos();
            return instancia;
        }

        private void FrmListadoPrestamos_Load(object sender, EventArgs e)
        {
            cargar_Listado();
        }

        public void cargar_Listado()
        {
            dgwPrestamo.DataSource = GestionPrestamo.list_Prestamos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAltaPrestamo = FrmAltaPrestamos.GetInstancia();
            frmAltaPrestamo.Show();
            this.Hide();
        }

        private void FrmListadoPrestamos_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmMain = FrmMain.GetInstancia();
            frmMain.Show();
            instancia = null;
        }
    }
}

