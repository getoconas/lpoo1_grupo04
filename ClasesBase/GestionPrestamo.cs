﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
    public class GestionPrestamo
    {
    	public static void insert_Prestamo(Prestamo prestamo)
    	{

    	}

    	public static void delete_Prestamo(Prestamo prestamo)
    	{

    	}

    	public static DataTable list_Prestamos()
    	{
    		SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT ";
            cmd.CommandText += "PRE_Numero AS 'ID', ";
            cmd.CommandText += "CLI_DNI AS 'DNI', ";
            cmd.CommandText += "DES_Codigo AS 'Cod_Des', ";
            cmd.CommandText += "PER_Codigo AS 'Cod_Per', ";
            cmd.CommandText += "PRE_Fecha AS 'Fecha', ";
            cmd.CommandText += "PRE_Importe AS 'Importe', ";
            cmd.CommandText += "PRE_TasaInteres AS 'Tasa_Interes' ";
            cmd.CommandText += "PRE_CantidadCuotas AS 'Cant_Cuota' ";
            cmd.CommandText += "PRE_Estado AS 'Estado' ";
            cmd.CommandText += "FROM Prestamo";

            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            //Ejecuta la consulta
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Llena los datos de la consulta en el Datatable
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
    	}
    }
}
