﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmAltaPrestamos : Form
    {
        private FrmGestionPrestamos frmListadoPrestamos;
        private Prestamo prestamo;
        private static FrmAltaPrestamos instancia;

        public FrmAltaPrestamos()
        {
            InitializeComponent();
            cargarListas();
        }

        public static FrmAltaPrestamos GetInstancia()
        {
            if (instancia == null)
                instancia = new FrmAltaPrestamos();
            return instancia;
        }

        private void cargarListas() 
        {
            Cliente cliente1 = new Cliente("11111111","sergio", "romero", "Masculino", DateTime.Parse("01/01/1996"),1500,"direccion 1","388-111111");
            Cliente cliente2 = new Cliente("22222222", "gaston", "toconas", "Masculino", DateTime.Parse("11/03/1994"), 2000, "direccion 2", "388-222222");
            Cliente cliente3 = new Cliente("33333333", "edmundo", "vasquez", "Masculino", DateTime.Parse("21/05/1992"), 3000, "direccion 3", "388-333333");

            cmbClientes.Items.Add(cliente1.Cli_Dni);
            cmbClientes.Items.Add(cliente2.Cli_Dni);
            cmbClientes.Items.Add(cliente3.Cli_Dni);

            /*cmbClientes.DisplayMember = "cli_Dni";
            cmbClientes.SelectedValue = "Cli_Dni";
            this.cmbClientes.DataSource = clientes;*/

            Periodo periodo1 = new Periodo(100,"periodo1");
            Periodo periodo2 = new Periodo(101, "periodo2");

            cmbPeriodo.Items.Add(periodo1.Per_Codigo);
            cmbPeriodo.Items.Add(periodo2.Per_Codigo);

            /*cmbPeriodo.DisplayMember = "per_Descripcion";
            cmbPeriodo.SelectedValue = "per_codigo";
            cmbPeriodo.DataSource = periodos;*/


            Destino destino1 = new Destino(100,"destino1");
            Destino destino2 = new Destino(101,"destino2");

            cmbDestino.Items.Add(destino1.Des_Codigo);
            cmbDestino.Items.Add(destino2.Des_Codigo);
            
            /*cmbDestino.DisplayMember = "des_Descripcion";
            cmbDestino.SelectedValue = "des_Codigo";
            this.cmbDestino.DataSource = destinos;*/

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!validarCamposVacios())
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Desea Agregar el prestamo?", "Agregar Prestamo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion.Equals(DialogResult.OK))
                {
                    agregarPrestamo();
                    MessageBox.Show(prestamo.ToString());
                }
                else
                    MessageBox.Show("Prestamo no registrado!!", "Agregar Prestamo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("Algunos Campos Estan Vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private bool validarCamposVacios()
        {
            return txtNumero.Text.Equals("") || txtCuotas.Text.Equals("") || txtImporte.Text.Equals("")
                || txtTasaInteres.Text.Equals("") || cmbClientes.SelectedIndex.Equals(-1) || cmbDestino.SelectedIndex.Equals(-1) || cmbPeriodo.SelectedIndex.Equals(-1);
        }

       

        private void agregarPrestamo()
        {
            prestamo = new Prestamo();
            prestamo.Pre_Numero = Convert.ToInt32(txtNumero.Text);
            prestamo.Pre_Estado = obtenerEstado();
            prestamo.Pre_Importe = Convert.ToDecimal(txtImporte.Text);
            prestamo.Pre_CantidadCuotas = Convert.ToInt32(txtCuotas.Text);
            prestamo.Pre_TasaInteres = Convert.ToDouble(txtTasaInteres.Text);
            prestamo.Pre_Fecha = dtpFecha.Value;
            prestamo.Des_Codigo =Convert.ToInt16( cmbDestino.Items[cmbDestino.SelectedIndex]);
            prestamo.Per_Codigo = Convert.ToInt16(cmbPeriodo.Items[cmbPeriodo.SelectedIndex]);
            prestamo.Cli_Dni = cmbClientes.Items[cmbClientes.SelectedIndex].ToString();
    
        }

        private string obtenerEstado()
        {
            string estado = null;
            if (rbCancelado.Checked)
                estado = "Cancelado";
            if (rbPendiente.Checked)
                estado = "Pendiente";
            return estado;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtImporte_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

        private void txtTasaInteres_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

        private void txtCuotas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

        private void FrmAltaPrestamos_FormClosing(object sender, FormClosingEventArgs e)
        {
            instancia = null;
            frmListadoPrestamos = FrmGestionPrestamos.GetInstancia();
            frmListadoPrestamos.Show();
        }
    }
}
