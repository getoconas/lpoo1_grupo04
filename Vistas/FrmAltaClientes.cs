﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClasesBase;

namespace Vistas
{
    public partial class FrmAltaClientes : Form
    {
        private Cliente cliente;
        private static FrmAltaClientes instancia;

        public static FrmAltaClientes GetInstancia()
        {
            if (instancia == null)
                instancia = new FrmAltaClientes();
            return instancia;
        }


        public FrmAltaClientes()
        {
            InitializeComponent();
        }

        private void txtDni_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))//No es numero y tecla borrar
                e.Handled = true;
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar)
                && !Char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar)
                && !Char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }


        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }

        private void txtIngresos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !(Convert.ToChar(Keys.Back) == e.KeyChar))
                e.Handled = true;
        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!validarCamposVacios())
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Desea Agregar al Cliente?", "Agregar Cliente", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion.Equals(DialogResult.OK))
                {
                    agregarCliente();
                    MessageBox.Show(cliente.ToString());
                }
                else
                    MessageBox.Show("Cliente no Registrado!!", "Agregar Cliente", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);    
            }
            else
                MessageBox.Show("Algunos Campos Estan Vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private bool validarCamposVacios() 
        {
            return txtDni.Text.Equals("") || txtApellido.Text.Equals("") || txtNombre.Text.Equals("")
                || txtDireccion.Text.Equals("") || txtIngresos.Text.Equals("");
        }

        private string obtenerSexo() 
        {
            string sexo = null;
            if (rbFemenino.Checked)
                sexo = "Femenino";
            if (rbMasculino.Checked)
                sexo = "Masculino";
            return sexo;
        }

        private void agregarCliente() 
        {
            cliente = new Cliente();
            cliente.Cli_Apellido = txtApellido.Text;
            cliente.Cli_Nombre = txtNombre.Text;
            cliente.Cli_Dni = txtDni.Text;
            cliente.Cli_Direccion = txtDireccion.Text;
            cliente.Cli_Telefono = txtTelefono.Text;
            cliente.Cli_FechaNacimiento = dtpFechaNacimiento.Value;
            cliente.Cli_Ingresos = Convert.ToDecimal(txtIngresos.Text);
            cliente.Cli_Sexo = obtenerSexo();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAltaClientes_Load(object sender, EventArgs e)
        {

        }

        private void FrmAltaClientes_FormClosing(object sender, FormClosingEventArgs e)
        {
            instancia = null;
        }
 
    }
}
